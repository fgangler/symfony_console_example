# Prerequisites

- [Composer](https://getcomposer.org/)
- **PHP 8**
- prerequisites for CI and developers:
  - Git
  - **PHIVE**

## PHIVE and QA tools

Usage on the project:

- [x] IDE
- [x] manual check
- [x] pre-commit check
- [x] CI check
- [ ] Deployment

see: [QA tools](QA_tools.md)

### Install PHIVE

[PHIVE documentation](https://phar.io)

```bash
# Install PHIVE
wget -O phive.phar https://phar.io/releases/phive.phar
wget -O phive.phar.asc https://phar.io/releases/phive.phar.asc
gpg --keyserver pool.sks-keyservers.net --recv-keys 0x9D8A98B29B2D5D79
gpg --verify phive.phar.asc phive.phar
chmod +x phive.phar
sudo mv phive.phar /usr/local/bin/phive
```

### Install QA tools

configuration file : [phive.xml](../phive.xml)

```bash
# Install all phar files declared in phive.xml
git clone <projectGitUrl>
cd <projectDirectory>

# Option 1
phive install --force-accept-unsigned

# Option 2
phive install php-parallel-lint/php-parallel-lint  --force-accept-unsigned --target ./bin/.phive
phive install
```

### GrumPHP - Registers Git hooks

- [GrumPHP](https://github.com/phpro/grumphp) will run some tests on the committed code.
- If the tests fail, you won't be able to commit your changes.
- configuration file : [grumphp.yml](../grumphp.yml)

```bash
# If necessary, backup your Git hooks directory
tar -czvf .git/hooks.tar.gz .git/hooks/

# Registers Git hooks
./bin/grumphp git:init

# Removes Git hooks
./bin/grumphp git:deinit

# To skip commit checks, add -n or --no-verify flag to commit command
git commit --no-verify -m "your commit message"
git commit -n          -m "your commit message"
```


-------------------

## PHP 8 on Ubuntu LTS

Usage on the project:

- [x] IDE
- [x] manual check
- [x] pre-commit check
- [x] CI check
- [x] Deployment

### Install PHP 8 on Ubuntu 18.04 or 20.04

```bash
# Install PHP 8
sudo apt install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php8.0
```

### Using PHP 8 on Ubuntu 18.04 or 20.04

```bash
# List installed PHP versions
sudo update-alternatives --list php

# Change PHP version
sudo update-alternatives --config php

# Check PHP version currently in use
php -v
```
